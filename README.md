# README #

Repozytorium REQMagazyn - Stowarzyszenie Inżynierii Wymagań

### Po co jest to repozytorium? ###

* Zawiera pliki do publikacji REQMagazyn (katalogi wydanie_01, wydanie_02 i _common_elements)
* Materiały SIW (logo SIW i projekty rollup)
* [REQMagazyn](http://req.wymagania.org.pl/)
* [SIW](http://wymagania.org.pl/)

### Jak zacząć? ###

* Oprogramowanie: Adobe InDesign CC, Photoshop CS6, Illustrator CS6
* Po ściągnięciu pliki umieścić w strukturze takiej jak struktura repozytorium
* Przy pierwszym otworzeniu każdego pliku .indd program może wyświetlić komunikat o brakujących łączach - jeśli struktura będzie analogiczna jak w repozytorium, po wskazaniu lokalizacji jednego z brakujących plików program powinien odnaleźć i naprawić pozostałe brakujące łącza

### Wskazówki ###

* Do stworzenia kolejnego wydania REQmagazyn najlepiej skopiować cały katalog wydanie_02 i na nim pracować, podmieniając teksty i obrazki
* Dla tekstów zastosowałam funkcję automatycznie przenoszącą wiszące spójniki, "sieroty i wdowy", itd - za pomocą styli GREP: `(?<=[ "][aiouwzAIOUWZ])` i `(?<=\w)\s(?=\w+[[:punct:]]+$)`
* Strony mają autonumerację; spis treści też mógłby być automatyczny ale przez layout numery stron w spisie treści wpisuję ręcznie
* Dla stron początkowych poszczególnych działów (kolorów) są utworzone strony wzorcowe; spis treści jest utworzony w stronie wzorcowej B-Spis-tresci (trzeba edytować stronę wzorcową do edycji spisu treści, nie wiem czemu tak mi wyszło, chyba przypadkiem i tak zostawiłam ;) )

### Kontakt ###

* dagmara.zawada@gmail.com